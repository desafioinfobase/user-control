# user-control

Api REST em Java para gerência de usuários e Aplicação Angular 8 para front-end.

Api REST Completa, porém ainda é necessário finalizar o projeto Angular.

**Usuário incial:**
`email: mail@example.com`
`senha: 123456`

**urls autenticação (Token JWT):**
`POST - /auth`
`POST - /auth/refresh`

**CRUD urls:**
`GET - /api/users`
`DELETE - /api/users/delete/{id}`
`POST - /api/users/register`
`PUT - /api/users/update/{id}`


